class Hangman
  attr_reader :guesser, :referee, :board

  def initialize(players)
    @guesser = players[:guesser]
    @referee = players[:referee]
    @board = nil
  end

  def setup
    length = referee.pick_secret_word
    @guesser.register_secret_length(length)
    @board = Array.new(length)
  end

  def take_turn
    guess = @guesser.guess(board)
    indices = @referee.check_guess(guess)
    update_board(indices, guess)
    @guesser.handle_response(guess, indices)
  end

  def update_board(guess, indices)
    indices.each { |idx| @board[idx] = guess }
  end

end

class HumanPlayer

  def initialize
  end

  def register_secret_length(length)
    puts "The secret word has #{length} letters."
  end

  def guess(board)
    puts board
    puts "Guess a letter: "
    gets.chomp
  end

  def handle_response(guess, indices)
    puts "Found #{guess} at position(s) #{indices}"
  end

end

class ComputerPlayer

  attr_reader :dictionary, :secret_word

  def initialize(dictionary)
    @dictionary = dictionary
  end

  def pick_secret_word
    @secret_word = dictionary.sample
    secret_word.length
  end

  def check_guess(letter)
    indices = []
    secret_word.chars.each_with_index do |char, idx|
      if char == letter
        indices << idx
      end
    end
    indices
  end

  def register_secret_length(length)
    @dictionary = dictionary.select { |word| word.length == length }
  end

  def guess(board)
    puts board
    rand_word = @dictionary.sample
    rand_word.chars.sample
  end

  def handle_response(guess, indices)

  end

  def candidate_words
    not_eliminated = @dictionary.map
    not_eliminated
  end

end
